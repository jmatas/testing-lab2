## Ejercicio 1:

Este ejercicio es bastante directo. Tras correr el test con las pruebas vemos que las pasamos con una cobertura del 100%.

```bash
> pytest --cov=g --cov-branch ejercicio1.py
========================================== test session starts ===========================================
platform linux -- Python 3.10.7, pytest-7.4.3, pluggy-1.3.0
rootdir: /home/pepe/Documentos/ISI/Modulo_4/testing
plugins: cov-4.1.0
collected 3 items                                                                                        

ejercicio1.py ...                                                                                  [100%]

---------- coverage: platform linux, python 3.10.7-final-0 -----------
Name    Stmts   Miss Branch BrPart  Cover
-----------------------------------------
g.py        6      0      4      0   100%
-----------------------------------------
TOTAL       6      0      4      0   100%


=========================================== 3 passed in 0.02s ============================================
```

## Ejercicio 2:

Si ejecutamos el test nos da error. Esto se debe a que si una lista tiene sólo dos elementos y el segundo no es menor que el primero, nunca se actualiza correctamente y sigue con su valor inicial de 0.

Tras corregir el código el test se ejecuta correctamente, pero con un 92% de cobertura.

```bash
pytest --cov=dos_menores --cov-branch ejercicio2.py
========================================== test session starts ===========================================
platform linux -- Python 3.10.7, pytest-7.4.3, pluggy-1.3.0
rootdir: /home/pepe/Documentos/ISI/Modulo_4/testing
plugins: cov-4.1.0
collected 4 items

ejercicio2.py ....                                                                                 [100%]

---------- coverage: platform linux, python 3.10.7-final-0 -----------
Name             Stmts   Miss Branch BrPart  Cover
--------------------------------------------------
dos_menores.py      14      1     12      1    92%
--------------------------------------------------
TOTAL               14      1     12      1    92%


=========================================== 4 passed in 0.02s ============================================
```

## Ejercicio 3:

### Tabla de casos

| Descripción                             | Datos de Entrada        | Resultado Esperado | Función de Test                      |
| --------------------------------------- | ----------------------- | ------------------ | ------------------------------------ |
| Lista con números positivos y negativos | [82, -3]                | 85                 | test_normal_case                     |
| Lista vacía                             | []                      | InvalidArgument    | test_invalid_argument_empty_list     |
| Lista con un solo elemento              | [1]                     | InvalidArgument    | test_invalid_argument_single_element |
| Lista con un elemento no entero         | [82, "no es un entero"] | TypeError          | test_type_error_non_integer          |
| Lista con varios elementos positivos    | [1, 3, 5, 7, 9]         | 2                  | test_positive_numbers                |
| Lista con mayor diferencia al final     | [2, 4, 6, 100]          | 94                 | test_largest_difference_at_end       |
| Lista con mayor diferencia al principio | [100, 3, 5, 7]          | 97                 | test_largest_difference_at_start     |
| Lista con todos los números iguales     | [5, 5, 5, 5]            | 0                  | test_all_same_numbers                |

Tras ejecutar:

```bash
> pytest ejercicio3.py --cov=max_dist --cov-branch 
================================================= test session starts ==================================================
platform linux -- Python 3.10.7, pytest-7.4.3, pluggy-1.3.0
rootdir: /home/pepe/Documentos/ISI/Modulo_4/testing
plugins: cov-4.1.0
collected 8 items                                                                                                      

ejercicio3.py ........                                                                                           [100%]

---------- coverage: platform linux, python 3.10.7-final-0 -----------
Name          Stmts   Miss Branch BrPart  Cover
-----------------------------------------------
max_dist.py      13      0      8      0   100%
-----------------------------------------------
TOTAL            13      0      8      0   100%


================================================== 8 passed in 0.03s ===================================================
```

Vemos que la cobertura es del 100%.

### Ejercicio 4

//TODO