import pytest
from dos_menores import dos_menores


@pytest.mark.parametrize("input, expected", [
    ([], None),  # Lista vacía
    ([1], 1),  # Un elemento
    ([1, 2], (1, 2)),  # Dos elementos
    ([3, 1, 4, 1, 5], (1, 1))  # Múltiples elementos
])
def test_dos_menores(input, expected):
    assert dos_menores(input) == expected


def main():
    test_dos_menores()
    print("All tests passed")


if __name__ == '__main__':
    main()
