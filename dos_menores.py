def dos_menores(a=None):
    if type(a) != list or len(a) < 1:
        return None

    if len(a) == 1:
        return a[0]

    # Inicializar min1 y min2 con los dos primeros elementos en el orden correcto
    if a[0] < a[1]:
        min1, min2 = a[0], a[1]
    else:
        min1, min2 = a[1], a[0]

    for e in a[2:]:
        if e < min1:
            min2, min1 = min1, e
        elif e < min2:
            min2 = e

    return min1, min2
