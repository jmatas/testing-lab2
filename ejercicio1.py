from g import g
import pytest


def test_g_even_number():
    assert g(4) == 1


def test_g_odd_number():
    assert g(3) == 1


def test_g_one():
    assert g(1) == 1


def main():
    test_g_even_number()
    test_g_odd_number()
    test_g_one()
    print("All tests passed")
    

if __name__ == '__main__':
    main()
