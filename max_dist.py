class InvalidArgument(Exception):
    "Function called with invalid arguments"
    pass


def max_dist(lst):
    if not isinstance(lst, list) or len(lst) < 2:
        raise InvalidArgument("La función debe recibir una lista con al menos dos enteros como argumento")

    max_difference = None

    for i in range(len(lst) - 1):
        if not isinstance(lst[i], int) or not isinstance(lst[i + 1], int):
            raise TypeError("La lista debe contener solo enteros")
        
        difference = abs(lst[i + 1] - lst[i])
        if max_difference is None or difference > max_difference:
            max_difference = difference

    return max_difference
