import pytest
from max_dist import max_dist, InvalidArgument


def test_normal_case():
    assert max_dist([82, -3]) == 85


def test_invalid_argument_empty_list():
    with pytest.raises(InvalidArgument):
        max_dist([])


def test_invalid_argument_single_element():
    with pytest.raises(InvalidArgument):
        max_dist([1])


def test_type_error_non_integer():
    with pytest.raises(TypeError):
        max_dist([82, "no es un entero"])


def test_positive_numbers():
    assert max_dist([1, 3, 5, 7, 9]) == 2


def test_largest_difference_at_end():
    assert max_dist([2, 4, 6, 100]) == 94


def test_largest_difference_at_start():
    assert max_dist([100, 3, 5, 7]) == 97


def test_all_same_numbers():
    assert max_dist([5, 5, 5, 5]) == 0
